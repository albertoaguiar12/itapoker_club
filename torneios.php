<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	require_once 'includes/Database.class.php'; //dando um require na classe de conexão com o banco
	$db = Database::conexao(); //realizando a conexão

	// Lampirkan db dan User
    require_once "includes/User.class.php";
    $user = new User($db);
    // Jika belum login
    if(!$user->isLoggedIn()){
        header("location: login.php"); //Redirect ke halaman login
    }
    // Ambil data user saat ini
    $currentUser = $user->getUser();

    require_once "includes/Crud.class.php";
    $crud = new Crud($db);

    
    /**
     * SELECIONANDO OS TORNEIOS
     */
    $torneios = $crud->selecionaTodosDados('torneios');


    /**
     * SELECIONANDO OS JOGADORES
     */
    $torneios = $db->query('SELECT * from torneios where excluido=0 ORDER BY id DESC');
    $torneios = $torneios->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Itapoker Clube - Admin</title>

	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="assets/style.css">

	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Itapoker Clube</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse" aria-expanded="true" style="">
					<ul class="nav navbar-nav">
						<li><a href="index.php">Ranking</a></li>
						<li><a href="jogadores.php">Jogadores</a></li>
						<li class="active"><a href="torneios.php">Torneios</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="includes/logout.php"><span class="glyphicon glyphicon-log-in"></span> Sair</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<section class="container tabela_raking">
		<a href="cadastra_torneio.php" class="btn btn-success pull-right" style="margin-bottom: 50px;">Adicionar Torneio</a>
		<br>

		<table class="table table-bordered text-center">
		    <thead>
				<tr>
					<th class="text-center">Nome</th>
					<th class="text-center">Ações</th>
				</tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($torneios as $key => $value) : ?>
		      	<tr>
		        	<td><?php echo $value['nome_torneio']; ?></td>
		        	<td><a href="#">Excluir</a> / <a href="editar_torneios.php?id_torneio=<?php echo $value['id']; ?>">Editar</a></td>
		      	</tr>
		      <?php endforeach; ?>
		    </tbody>
		</table>
	</section>
</body>
</html>