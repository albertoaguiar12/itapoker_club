-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 19/06/2018 às 20:50
-- Versão do servidor: 5.7.22-0ubuntu0.16.04.1
-- Versão do PHP: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `itapoker_clube`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `jogadores`
--

CREATE TABLE `jogadores` (
  `id` int(11) NOT NULL,
  `nome_jogador` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rg_jogador` varchar(15) NOT NULL,
  `excluido` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `jogadores`
--

INSERT INTO `jogadores` (`id`, `nome_jogador`, `rg_jogador`, `excluido`) VALUES
(1, 'Alberto', '111111111', 0),
(2, 'Vadinho', '222222222', 0),
(3, 'Lorenzo', '333333333', 0),
(4, 'Renato', '444444444', 0),
(5, 'Augusto', '555555555', 0),
(6, 'Beto', '666666666', 0),
(7, 'Antonio', '12122', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `lig_torneios_jogadores`
--

CREATE TABLE `lig_torneios_jogadores` (
  `id` int(11) NOT NULL,
  `fk_torneios` int(11) NOT NULL,
  `fk_jogadores` int(11) NOT NULL,
  `pontuacao_jogador` int(11) NOT NULL,
  `excluido` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `lig_torneios_jogadores`
--

INSERT INTO `lig_torneios_jogadores` (`id`, `fk_torneios`, `fk_jogadores`, `pontuacao_jogador`, `excluido`) VALUES
(1, 1, 1, 15000, 0),
(2, 1, 2, 13453, 0),
(3, 1, 3, 12322, 0),
(4, 1, 4, 10000, 0),
(5, 1, 5, 2344, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `torneios`
--

CREATE TABLE `torneios` (
  `id` int(11) NOT NULL,
  `nome_torneio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vencedor_torneio` int(11) DEFAULT NULL,
  `excluido` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `torneios`
--

INSERT INTO `torneios` (`id`, `nome_torneio`, `vencedor_torneio`, `excluido`) VALUES
(1, 'Torneio principal', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `torneio_etapas`
--

CREATE TABLE `torneio_etapas` (
  `id` int(11) NOT NULL,
  `fk_torneios` int(11) NOT NULL,
  `numero_etapa` int(100) NOT NULL,
  `vencedor_etapa` int(11) NOT NULL,
  `excluido` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `torneio_etapas`
--

INSERT INTO `torneio_etapas` (`id`, `fk_torneios`, `numero_etapa`, `vencedor_etapa`, `excluido`) VALUES
(1, 1, 1, 1, 0),
(2, 1, 2, 3, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome_usuario` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome_usuario`, `login`, `password`, `token`) VALUES
(1, 'Rodolfo', 'rodolfo', '202cb962ac59075b964b07152d234b70', '111');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `jogadores`
--
ALTER TABLE `jogadores`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `lig_torneios_jogadores`
--
ALTER TABLE `lig_torneios_jogadores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_torneios` (`fk_torneios`),
  ADD KEY `fk_jogadores` (`fk_jogadores`);

--
-- Índices de tabela `torneios`
--
ALTER TABLE `torneios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vencedor_torneio` (`vencedor_torneio`);

--
-- Índices de tabela `torneio_etapas`
--
ALTER TABLE `torneio_etapas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_torneios` (`fk_torneios`),
  ADD KEY `vencedor_etapa` (`vencedor_etapa`);

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `jogadores`
--
ALTER TABLE `jogadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de tabela `lig_torneios_jogadores`
--
ALTER TABLE `lig_torneios_jogadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `torneios`
--
ALTER TABLE `torneios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `torneio_etapas`
--
ALTER TABLE `torneio_etapas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `lig_torneios_jogadores`
--
ALTER TABLE `lig_torneios_jogadores`
  ADD CONSTRAINT `fk_torneios_jogadores_lig` FOREIGN KEY (`fk_jogadores`) REFERENCES `jogadores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_torneios_lig` FOREIGN KEY (`fk_torneios`) REFERENCES `torneios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `torneios`
--
ALTER TABLE `torneios`
  ADD CONSTRAINT `fk_vencedor_torneio` FOREIGN KEY (`vencedor_torneio`) REFERENCES `jogadores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `torneio_etapas`
--
ALTER TABLE `torneio_etapas`
  ADD CONSTRAINT `fk_torneios_etapas` FOREIGN KEY (`fk_torneios`) REFERENCES `torneios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_torneios_etapas_vencedor` FOREIGN KEY (`vencedor_etapa`) REFERENCES `jogadores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
