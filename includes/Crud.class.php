<?php

class Crud
{

	private $db;
    
    function __construct($db_conn)
    {
        $this->db = $db_conn;
    }

	public function atualizarDados($tabela,$update,$where)
	{
		if($this->db->query('UPDATE '.$tabela.' SET '.$update.' WHERE '.$where)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function selecionaTodosDados($tabela)
	{
		$dados = $this->db->query('SELECT * FROM '.$tabela.' ORDER BY id DESC');
    	$dados = $dados->fetchAll(PDO::FETCH_ASSOC);

    	return $dados;
	}

	public function excluirDado($id)
	{
		return 'método que exclui o dado da tabela com base no id';
	}

	public function insereDados($tabela,$campos,$values) {
		if($this->db->query('INSERT INTO '.$tabela.' ('.$campos.') VALUES ('.$values.')')) {
			if($tabela == 'jogadores') {
				echo "<script>alert('O jogador foi cadastrado com sucesso!'); window.location = 'jogadores.php';</script>";
				return TRUE;
			}

			if($tabela == 'torneios') {
				echo "<script>alert('O torneio foi cadastrado com sucesso!'); window.location = 'torneios.php';</script>";
				return TRUE;
			}
		} else {
			if($tabela == 'jogadores') {
				echo "<script>alert('Erro ao realizar o cadastro.')</script>";
			}
			return FALSE;
		}
	}
}