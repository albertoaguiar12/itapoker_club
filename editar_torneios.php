<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	require_once 'includes/Database.class.php'; //dando um require na classe de conexão com o banco
	$db = Database::conexao(); //realizando a conexão

	// Lampirkan db dan User
    require_once "includes/User.class.php";
    $user = new User($db);
    // Jika belum login
    if(!$user->isLoggedIn()){
        header("location: login.php"); //Redirect ke halaman login
    }
    // Ambil data user saat ini
    $currentUser = $user->getUser();

    require_once "includes/Crud.class.php";
    $crud = new Crud($db);

    $torneios = $db->query('SELECT * from torneios where excluido=0 and id='.$_GET['id_torneio'].' ORDER BY id DESC');
    $torneios = $torneios->fetchAll(PDO::FETCH_ASSOC);


    $jogadores_vinculados = $db->query('SELECT jgd.id, jgd.nome_jogador FROM lig_torneios_jogadores ltj LEFT JOIN jogadores jgd ON ltj.fk_jogadores = jgd.id WHERE ltj.fk_torneios='.$_GET['id_torneio']);
    $jogadores_vinculados = $jogadores_vinculados->fetchAll(PDO::FETCH_ASSOC);


    $jogadores_nao_vinculados = $db->query('SELECT jgd.nome_jogador, jgd.id FROM jogadores jgd WHERE jgd.id NOT IN (SELECT fk_jogadores FROM lig_torneios_jogadores WHERE fk_torneios='.$_GET['id_torneio'].')');
    $jogadores_nao_vinculados = $jogadores_nao_vinculados->fetchAll(PDO::FETCH_ASSOC);

    /**
     * INSERINDO JOGADORES NO BANCO
     */
    if(isset($_POST['nome_torneio'])) {
    	$campos = 'nome_torneio';
    	$values = '"'.$_POST['nome_torneio'].'"';
    	$torneios = $crud->insereDados('torneios',$campos,$values);

    	if($torneios) {
    		echo "<script>Torneio cadastrado com sucesso!</script>";
    	} else {
    		echo "<script>Ocorreu algum erro ao cadastrar o torneio!</script>";
    	}
    }

    // $torneios = $torneios->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Itapoker Clube - Admin</title>

	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="assets/style.css">

	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Itapoker Clube</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse" aria-expanded="true" style="">
					<ul class="nav navbar-nav">
						<li><a href="index.php">Ranking</a></li>
						<li><a href="jogadores.php">Jogadores</a></li>
						<li class="active"><a href="torneios.php">Torneios</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="includes/logout.php"><span class="glyphicon glyphicon-log-in"></span> Sair</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<section class="container tabela_raking">
		<h1>Cadastro de torneios</h1>
		<br>

		<div class="col-md-6">
			<form action="cadastra_torneio.php" method="POST">
				<div class="form-group">
					<input type="text" placeholder="Nome" name="nome_torneio" value="<?php echo $torneios[0]['nome_torneio']; ?>">
				</div>
				<button type="submit" class="btn btn-success">Salvar jogador</button>
			</form>
		</div>

		<div class="col-md-6">
			<label for="sel2">Vincular jogadores com o torneio "<?php echo $torneios[0]['nome_torneio']; ?>"</label>
			<div class="col-md-6">
				<select multiple class="form-control" style="height: 250px;">
					<?php foreach ($jogadores_nao_vinculados as $key => $value) : ?>
						<option value="<?php echo $value['id']; ?>"><?php echo $value['nome_jogador']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-md-6">
				<select multiple class="form-control" style="height: 250px;">
					<?php foreach ($jogadores_vinculados as $key => $value) : ?>
						<option value="<?php echo $value['id']; ?>"><?php echo $value['nome_jogador']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
	</section>
</body>
</html>