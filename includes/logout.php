<?php
    // Lampirkan db dan User
    require_once "Database.class.php";
    $db = Database::conexao(); //realizando a conexão
    require_once "User.class.php";
    // Buat object user
    $user = new User($db);
    // Logout! hapus session user
    $user->logout();
    // Redirect ke login
    header('location: ../login.php');
 ?>