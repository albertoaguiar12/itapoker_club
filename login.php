<?php
    // Lampirkan db dan User
    require_once "includes/Database.class.php";
    $db = Database::conexao(); //realizando a conexão
    require_once "includes/User.class.php";
    //Buat object user
    $user = new User($db);
    //Jika sudah login
    if($user->isLoggedIn()){
        header("location: index.php"); //redirect ke index
    }
    //jika ada data yg dikirim
    if(isset($_POST['kirim'])){
        $login = $_POST['login'];
        $password = $_POST['password'];
        // Proses login user
        if($user->login($login, $password)){
            header("location: index.php");
        }else{
            $error = $user->getLastError();
        }
    }
 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Itapoker Clube</title>

	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="assets/style.css">

	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body class="body_login">
	<section id="login">
	    <div class="container">
	    	<div class="row">
	    	    <div class="col-xs-12">
	        	    <div class="form-wrap" style="margin-top: 50px;">
	        	    	<img class="img-responsive" src="assets/imgs/logo.jpeg">
	                    <form role="form" action="" method="post" id="login-form" autocomplete="off" style="margin-top: 30px;">
	                        <div class="form-group">
	                            <label class="sr-only">Email</label>
	                            <input type="text" name="login" id="login" class="form-control" placeholder="Login">
	                        </div>
	                        <div class="form-group">
	                            <label for="key" class="sr-only">Password</label>
	                            <input type="password" name="password" id="key" class="form-control" placeholder="Senha">
	                        </div>
	                        <?php if (isset($error)): ?>
			                  <div class="alert-danger" style="padding: 5px; border-radius: 5px;">
			                      <?php echo $error ?>
			                  </div>
			              <?php endif; ?>
	                        <div class="checkbox">
	                            <span class="character-checkbox" onclick="showPassword()"></span>
	                            <span class="label">Mostrar senha</span>
	                        </div>
	                        <input name="kirim" type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Entrar">
	                    </form>
	                    <hr>
	        	    </div>
	    		</div> <!-- /.col-xs-12 -->
	    	</div> <!-- /.row -->
	    </div> <!-- /.container -->
	</section>

	<footer id="footer">
	    <div class="container">
	    	<hr>
	        <div class="row">
	            <div class="col-xs-12">
	                <p>Desenvolvido por <strong><a href="https://twitter.com/walbertolino" target="new">Alberto Aguiar</a></strong></p>
	            </div>
	        </div>
	    </div>
	</footer>


	<script src="assets/common.js"></script>
</body>
</html>