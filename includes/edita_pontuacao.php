<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'Database.class.php'; //dando um require na classe de conexão com o banco
$db = Database::conexao(); //realizando a conexão

require_once "Crud.class.php";
$crud = new Crud($db);

header("Content-Type: application/json");


if(isset($_POST['pontuacoes'])){

  	$dados = json_decode($_POST['pontuacoes'], true);
  
	foreach ($dados as $key => $value) {
  		$where = 'fk_torneios='.$value['id_torneio'].' and fk_jogadores='.$value['id_jogador'];
  		$update = 'pontuacao_jogador='.$value['pontuacao_total'];
  		
  		if($crud->atualizarDados('lig_torneios_jogadores',$update,$where)) {
  			echo TRUE;
  		} else {
  			echo FALSE;
  		}
	}
}