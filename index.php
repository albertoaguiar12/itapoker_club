<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	require_once 'includes/Database.class.php'; //dando um require na classe de conexão com o banco
	$db = Database::conexao(); //realizando a conexão

	// Lampirkan db dan User
    require_once "includes/User.class.php";
    $user = new User($db);
    // Jika belum login
    if(!$user->isLoggedIn()){
        header("location: login.php"); //Redirect ke halaman login
    }
    // Ambil data user saat ini
    $currentUser = $user->getUser();

    require_once "includes/Crud.class.php";
    $crud = new Crud($db);

    
    /**
     * SELECIONANDO OS TORNEIOS
     */
    $torneios = $crud->selecionaTodosDados('torneios');


    /**
     * INSERINDO POTUAÇÃO NO BANCO
     */


    /**
     * SELECIONANDO OS JOGADORES
     */
    $jogadores = $db->query('SELECT ltj.*, jog.nome_jogador, jog.id as id_jogador FROM lig_torneios_jogadores ltj LEFT JOIN jogadores jog on jog.id=ltj.fk_jogadores ORDER BY pontuacao_jogador DESC');
    $jogadores = $jogadores->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Itapoker Clube - Admin</title>

	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="assets/style.css">

	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Itapoker Clube</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse" aria-expanded="true" style="">
					<ul class="nav navbar-nav">
						<li class="active"><a href="index.php">Ranking</a></li>
						<li><a href="jogadores.php">Jogadores</a></li>
						<li><a href="torneios.php">Torneios</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="includes/logout.php"><span class="glyphicon glyphicon-log-in"></span> Sair</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<section class="container tabela_raking">
		<div class="form-group" style="width: 300px;">
			<label>Selecione o torneio:</label>
			<select class="form-control torneio_selecionado">
				<?php foreach ($torneios as $key => $value) : ?>
				<option value="<?php echo $value['id']; ?>"><?php echo $value['nome_torneio']; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<br>

		<table class="table table-bordered text-center">
		    <thead>
				<tr>
					<th class="text-center">Posição</th>
					<th class="text-center">Nome</th>
					<th class="text-center">Pontuação</th>
					<th class="text-center">Adicionar Pontos</th>
					<th class="text-center">Pontuação Parcial</th>
				</tr>
		    </thead>
		    <tbody>
		    	<?php $posicao=0; ?>
		    	<?php foreach ($jogadores as $key => $value) : ?>
		      	<tr id-jogador="<?php echo $value['id_jogador'] ?>">
		        	<td class="hidden id_jogador"><?php echo $value['id_jogador']; ?></td>
		        	<td><?php echo ++$posicao; ?></td>
		        	<td><?php echo $value['nome_jogador']; ?></td>
		        	<td><?php echo $value['pontuacao_jogador']; ?></td>
		        	<td>
		        		<input class="input_somar_ponto" type="text" value="0" data-jogador="<?php echo $value['id_jogador'] ?>">
		        	</td>
		        	<td class="pontuacao_parcial"><?php echo $value['pontuacao_jogador']; ?></td>
		      	</tr>
		      <?php endforeach; ?>
		    </tbody>
		</table>

		<button id="salvarPontuacao" type="button" class="btn btn-success pull-right">Salvar pontuação</button>
	</section>


	<script>
		$('.input_somar_ponto').change(function () {
			console.log();
			let pontuacao_adicionar = $(this).val();
			let pontuacao_atual = $(this).closest('[id-jogador]').find('.pontuacao_parcial').text();

			let soma_pontos = parseInt(pontuacao_atual) + parseInt(pontuacao_adicionar);

			$(this).closest('[id-jogador]').find('.pontuacao_parcial').text(soma_pontos); //coloca o valor somado na pontuação parcial
		});

		$('#salvarPontuacao').click(function() {
			let dados = {};

			$('[id-jogador]').each(function(key) {
				let valor_somar = $(this).find('.input_somar_ponto').val();

				let pontuacao_total = $(this).find('.pontuacao_parcial').text();
				let id_jogador = $(this).find('.id_jogador').text();
				let id_torneio = $('.torneio_selecionado').val();

				key = key+1;
				if(valor_somar != 0) {
					dados[key] = {id_jogador:id_jogador,pontuacao_total:pontuacao_total,id_torneio:id_torneio};
				}
			});


			 $.ajax({
			    url: "./includes/edita_pontuacao.php",
			    type: "post",
			    data: {pontuacoes: JSON.stringify(dados)},
			    dataType: 'json',
			    success: function (sucesso) {
			        swal({
			        	title: "Sucesso!",
						text: "A pontuação foi atualizada com sucesso!",
						type: "success"
					}).then(okay => {
						if (okay) {
							window.location.href = "index.php";
						}
					});
			    },
			    error: function(erro) {
			    	console.error(erro);
			    }
			});
		});
	</script>
</body>
</html>